import flask
from flask_sqlalchemy import SQLAlchemy
from flaskr.config import config_data
from flask_migrate import Migrate
# from flask.ext.login import LoginManager
import flask_login

app = flask.Flask(__name__)
app.config.from_object(__name__)
app.config.update(config_data)
app.config.from_envvar("FLASKR_SETTINGS", silent=True)

db = SQLAlchemy(app)
migrate = Migrate(app, db)

login_manager = flask_login.LoginManager()
login_manager.init_app(app)


users = {'foo@bar.tld': {'pw': 'secret'}}


class User(flask_login.UserMixin):
    pass


@login_manager.user_loader
def user_loader(email):
    if email not in users:
        return

    user = User()
    user.id = email
    return user


@login_manager.request_loader
def request_loader(request):
    email = request.form.get('email')
    if email not in users:
        return

    user = User()
    user.id = email
    user.is_authenticated = request.form['pw'] == users[email]['pw']

    return user


@app.route("/")
def main():
    return flask.render_template('index.html')


@app.route("/sign-in", methods=['GET', 'POST'])
def signin():
    if flask.request.method == 'GET':
        return flask.render_template('signin.html')

    email = flask.request.form['email']
    if flask.request.form['pw'] == users[email]['pw']:
        user = User()
        user.id = email
        flask_login.login_user(user)
        return flask.redirect(flask.url_for('protected'))

    return 'Bad login'


@app.route('/protected')
@flask_login.login_required
def protected():
    return flask.render_template('index.html')


@app.route("/sign-up")
def signup():
    pass


@app.route('/logout')
def logout():
    flask_login.logout_user()
    return flask.render_template('index.html')


@login_manager.unauthorized_handler
def unauthorized_handler():
    return 'Unauthorized'


if __name__ == "__main__":
    app.run()
