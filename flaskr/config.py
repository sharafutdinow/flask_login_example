import os

config_data = dict(
    SECRET_KEY='AfYpSk]Z%hdvUfvLsy6VSwV^9HYg{&8',
    SQLALCHEMY_DATABASE_URI='mysql://root:root@localhost/bucketlist',
    SQLALCHEMY_TRACK_MODIFICATIONS=True,
    UPLOAD_FOLDER=os.path.abspath('')+'/uploads',
    MEDIA_FOLDER=os.path.abspath('')+'/uploads',
    ALLOWED_EXTENSIONS=['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'],
)
